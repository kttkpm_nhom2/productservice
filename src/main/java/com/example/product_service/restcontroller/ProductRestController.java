package com.example.product_service.restcontroller;

import com.example.product_service.dto.ProductDto;
import com.example.product_service.dto.ProductDtoDetail;
import com.example.product_service.entity.Product;
import com.example.product_service.repository.ProductRepository;
import com.example.product_service.service.ProductService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductRestController {
    @Autowired
    private ProductService productService;
    @Autowired
    ProductRepository productRepository;
    private int soLanGoi=1;

    @GetMapping("/products")
    public List<Product> getAllProduct() {
        return productService.getAllProduct();
    }

    @Cacheable(value = "product",key = "#id")
    @GetMapping("/product/{id}")
    public Product getProductByID(@PathVariable int id) {
        System.out.println("Load productByID from DB");
        return productService.getProductByID(id);
    }

    @PostMapping("/product")
    public Product addNewProduct(@RequestBody Product product) {
        return productService.addProduct(product);
    }

    @PutMapping("/product")
    @CachePut(value = "product",key = "#productCurrent.productID")
    public Product updateProduct(@RequestBody Product productCurrent) {

        return productService.updateProduct(productCurrent);
    }

    @DeleteMapping("/product/{id}")
    @CacheEvict(value = "product",allEntries = true)
    public String deleteProductByID(@PathVariable int id) {
        return productService.deleteProductByID(id);
    }

    @GetMapping("/productDtos/highlightProducts")
    @CircuitBreaker(name = "productService")
    public List<ProductDto> getProductHightlight() {
        return productService.getProductDtoHightlight();
    }
    @GetMapping("/productDtos/newProducts")
    @Retry(name = "productService")
    public List<ProductDto> getPnewProducts() {
        System.out.println("Gọi api getNewProduct lần : "+soLanGoi);
        soLanGoi++;
        return productService.getProductDtoHNew();
    }
    public ProductDtoDetail rateLimitterFallBack (Exception e){
        Date date =new Date();
        ProductDtoDetail productDtoDetail =new ProductDtoDetail();
        productDtoDetail.setName("Quá nhiều request");
        productDtoDetail.setMoTa("Quá nhiều request");
        productDtoDetail.setCreated_at(date);
        productDtoDetail.setUpdated_at(date);


        return productDtoDetail;
    }
    @GetMapping("/productDetailDto/{id}")
    @RateLimiter(name = "productService",fallbackMethod = "rateLimitterFallBack")
    public ProductDtoDetail getProductDtoDetailByID(@PathVariable int id) {
        return productService.getProductDtoDetailByID(id);
    }
    @GetMapping("/productDtosByCategory/{id}")
    public List<ProductDto> getProductDtoBycategoryID(@PathVariable int id) {
        return productService.getProductDtoByCategoryID(id);
    }
//    @GetMapping("/getProductHightlight")
//    public List<Product> test1(){
//        return productRepository.getProductHightlight();
//    }
//    @GetMapping("/getProductNew")
//    public List<Product> test2(){
//        return productRepository.getProductNew();
//    }

}

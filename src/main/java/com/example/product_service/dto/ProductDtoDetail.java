package com.example.product_service.dto;

import java.util.Date;
import java.util.List;

public class ProductDtoDetail {
    private int productID;
    private String name;
    private int price;
    private boolean hightLight;
    private boolean newProduct;
    private String moTa;
    private int soLuong;
    private Date created_at;
    private Date updated_at;
    private int categoryID;
    private List<Color> listColor;

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isHightLight() {
        return hightLight;
    }

    public void setHightLight(boolean hightLight) {
        this.hightLight = hightLight;
    }

    public boolean isNewProduct() {
        return newProduct;
    }

    public void setNewProduct(boolean newProduct) {
        this.newProduct = newProduct;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public List<Color> getListColor() {
        return listColor;
    }

    public void setListColor(List<Color> listColor) {
        this.listColor = listColor;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public ProductDtoDetail(int productID, String name, int price, boolean hightLight, boolean newProduct, String moTa, int soLuong, Date created_at, Date updated_at, int categoryID, List<Color> listColor) {
        this.productID = productID;
        this.name = name;
        this.price = price;
        this.hightLight = hightLight;
        this.newProduct = newProduct;
        this.moTa = moTa;
        this.soLuong = soLuong;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.categoryID = categoryID;
        this.listColor = listColor;
    }

    public ProductDtoDetail() {
    }

    @Override
    public String toString() {
        return "ProductDtoDetail{" +
                "productID=" + productID +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", hightLight=" + hightLight +
                ", newProduct=" + newProduct +
                ", moTa='" + moTa + '\'' +
                ", soLuong=" + soLuong +
                ", created_at=" + created_at +
                ", updated_at=" + updated_at +
                ", categoryID=" + categoryID +
                ", listColor=" + listColor +
                '}';
    }
}

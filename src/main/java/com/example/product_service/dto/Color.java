package com.example.product_service.dto;

public class Color {
    private int colorID;
    private String name;
    private String code;
    private int productID;
    private String img;

    public int getColorID() {
        return colorID;
    }

    public void setColorID(int colorID) {
        this.colorID = colorID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getProductID() {
        return productID;
    }


    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Color(int colorID, String name, String code, int productID, String img) {
        this.colorID = colorID;
        this.name = name;
        this.code = code;
        this.productID = productID;
        this.img = img;
    }

    public Color() {
    }

    @Override
    public String toString() {
        return "Color{" +
                "colorID=" + colorID +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", productID=" + productID +
                ", img='" + img + '\'' +
                '}';
    }
}

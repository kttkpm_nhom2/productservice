package com.example.product_service.dto;

import java.util.Date;

public class ProductDto {
    private int productID;
    private String name;
    private int price;
    private boolean hightLight;
    private boolean newProduct;
    private String moTa;
    private int soLuong;
    private Date created_at;
    private Date updated_at;
    private int categoryID;
    private int colorID;
    private String nameColor;
    private String codeColor;
    private String imageColor;

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isHightLight() {
        return hightLight;
    }

    public void setHightLight(boolean hightLight) {
        this.hightLight = hightLight;
    }

    public boolean isNewProduct() {
        return newProduct;
    }

    public void setNewProduct(boolean newProduct) {
        this.newProduct = newProduct;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int getColorID() {
        return colorID;
    }

    public void setColorID(int colorID) {
        this.colorID = colorID;
    }

    public String getNameColor() {
        return nameColor;
    }

    public void setNameColor(String nameColor) {
        this.nameColor = nameColor;
    }

    public String getCodeColor() {
        return codeColor;
    }

    public void setCodeColor(String codeColor) {
        this.codeColor = codeColor;
    }

    public String getImageColor() {
        return imageColor;
    }

    public void setImageColor(String imageColor) {
        this.imageColor = imageColor;
    }

    public ProductDto(int productID, String name, int price, boolean hightLight, boolean newProduct, String moTa, int soLuong, Date created_at, Date updated_at, int categoryID, int colorID, String nameColor, String codeColor, String imageColor) {
        this.productID = productID;
        this.name = name;
        this.price = price;
        this.hightLight = hightLight;
        this.newProduct = newProduct;
        this.moTa = moTa;
        this.soLuong = soLuong;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.categoryID = categoryID;
        this.colorID = colorID;
        this.nameColor = nameColor;
        this.codeColor = codeColor;
        this.imageColor = imageColor;
    }

    public ProductDto() {
    }

    @Override
    public String toString() {
        return "ProductDto{" +
                "productID=" + productID +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", hightLight=" + hightLight +
                ", newProduct=" + newProduct +
                ", moTa='" + moTa + '\'' +
                ", soLuong=" + soLuong +
                ", created_at=" + created_at +
                ", updated_at=" + updated_at +
                ", categoryID=" + categoryID +
                ", colorID=" + colorID +
                ", nameColor='" + nameColor + '\'' +
                ", codeColor='" + codeColor + '\'' +
                ", imageColor='" + imageColor + '\'' +
                '}';
    }
}
